const User = require("../models/User.model");
const bcrypt = require("bcrypt");
const HTTPCODE = require("../utils/httpsStatusCode");
const upload = require('../services/updateProfilePic.service');
const profileUpload = upload.single('image');

module.exports = {
  getAllUsers: async (req, res, next) => {
    try {
      if (req.query.page) {
        //Se le añade paginación
        const page = parseInt(req.query.page);
        const skip = (page - 1) * 20;
        const Users = await User.find()
          .skip(skip)
          .limit(20)
          .populate("coworkings");


        return res.json({
          status: 200,
          message: HTTPCODE[200],
          data: { Users: Users },
        });
      } else {
        const Users = await User.find().populate("coworkings");
        const totalUsers = await  User.count()  //TOTAL USER
        return res.json({
          status: 200,
          message: HTTPCODE[200],
          data: { Users: Users,
          totalUsers },
        });
      }
    } catch (err) {
      return next(err);
    }
  },

  getUserById: async (req, res, next) => {
    try {
      const { id } = req.params;
      const userById = await User.findById(id).populate("coworkings");
      return res.json({
        status: 200,
        message: HTTPCODE[200],
        data: { User: userById },
      });
    } catch (err) {
      return next(err);
    }
  },


  newUser: async (req, res, next) => {


    try {
      const { rol, username, password, email, ...all } = req.body;
      const newUser = new User({email, rol, username, password, ...all });
      //Bcrypt
      const salt = bcrypt.genSaltSync(10);
      newUser.password = bcrypt.hashSync(password, salt);
      const userDb = await newUser.save();
      return res.json({
        status: 201,
        message: HTTPCODE[201],
        data: { newUser: userDb,
        //   all: { ...all },
        //   allSinSpread: { all },
        //   allPass: all.password,
        //   allAddress: all.address,

         },
      });



    } catch (err) {
      return next(err);
    }
  },

  deleteUser: async (req, res, next) => {
    try {
      const { id } = req.params;
      const userDeleted = await User.findByIdAndDelete(id);
      if (!userDeleted) {
        return res.json({
          status: 404,
          message: HTTPCODE[404],
          data: null,
        });
      } else {
        return res.json({
          status: 200,
          message: HTTPCODE[200],
          data: { userDeleted: userDeleted },
        });
      }
    } catch (err) {
      return next(err);
    }
  },

  updateUserById: async (req, res, next) => {
    try {
      const { id } = req.params;
      console.log("Esto ID==>",id);
      console.log( "Esto es lo del BACK==>  1",req.body);

     const updateUser= await User.findByIdAndUpdate(id,req.body, { new: true });

     console.log("Esto es lo del BACK  2==>",updateUser);
      return res.json({
        status: 200,
        message: HTTPCODE[200],
        data: { update: updateUser },
      });
    } catch (err) {
      return next(err);
    }
  },

  updateProfilePic: async (req, res, next) => {
    try {
        const { id } = req.params;
        console.log("Body de Juan",req.body);
        profileUpload(req, res, function(err) {
          console.log("Primero en el BACK",id);
                const user = async (id) => User.findByIdAndUpdate(id, {imgProfile: req.file.location}, {new: true});
                user(id)
                return res.json({
                    status: 200,
                    message: HTTPCODE[200],
                    data: {imgProfile: req.file.location}
                })
        })
    } catch(err) {
        return next(err);
}
}
};
