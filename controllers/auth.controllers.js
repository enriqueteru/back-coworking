const User = require("../models/User.model");
const HTTPCODE = require("../utils/httpsStatusCode");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const { googleVerify } = require("../helpers/google-verify");
const { response } = require("express");


module.exports = {
  registerAuth: async (req, res, next) => {
    try {
      const {
        username,
        name,
        surname,
        docIdentity,
        imgProfile,
        email,
        address,
        phone,
        enterprise,
        password,
        role
      } = req.body;

      const checkUser = await User.findOne({ email });
      if (checkUser) {
        return res.json({
          status: 400,
          message: HTTPCODE[400],
          data: { data: checkUser.email },
        });
      }
      if (!checkUser) {
        const newUser = new User({
          username,
          name,
          surname,
          docIdentity,
          imgProfile,
          email,
          address,
          phone,
          enterprise,
          password,
          role
        });

        const salt = bcrypt.genSaltSync(10);
        newUser.password = bcrypt.hashSync(password, salt);
        const savedUser = await newUser.save();

        const token = jwt.sign(
          {
            uid: savedUser._id,
            username: savedUser.username,
            name: savedUser.name,
            role: savedUser.role,
            coworkings: savedUser.coworkings,
            enterprise: savedUser.enterprise
          },
          req.app.get("secretKey"),
          { expiresIn: "12h" }
        );

        return res.json({
          status: 201,
          message: HTTPCODE[201],
          data: { savedUser: savedUser,
            token: token
           },
        });
      }
    } catch (err) {
      return next(err);
    }
  },

  authenticatedAuth: async (req, res, next) => {
    try {
      // Take password and email from body;
      const { email, password } = req.body;
      // Find email in DDBB;
      const user = await User.findOne({ email });
      // Compare the passwors body and password user
      const isValid = await bcrypt.compare(password, user.password);
      if (isValid) {
        user.password = null;


        const token = jwt.sign(
          {
            uid: user._id,
            username: user.username,
            name: user.name,
            role: user.role,
            coworkings: user.coworkings,
            enterprise: user.enterprise
          },
          req.app.get("secretKey"),
          { expiresIn: "12h" }
        );

        return res.json({
          status: 200,
          message: HTTPCODE[200],
          data: { token: token, user: user },
        });
      } else {
        return res.json({
          status: 400,
          message: HTTPCODE[400],
          data: null,
        });
      }
    } catch (err) {
      return next(err);
    }
  },
  logoutAuth: async (req, res, next) => {
    try {
      return res.json({
        status: 200,
        message: HTTPCODE[200],
        token: null,
      });
    } catch (err) {
      return next(err);
    }
  },

//mantener abierta la sesión en front
renewToken: async(req, res) => {
const authority = req.authority
console.log(authority);
    return res.json({
      status: 200,
      message: "Token renew",
      data: {
        authority
      }

  })},


  googleSignIn: async (req, res = response, next) => {
    const { google_token } = req.body;
    // console.log(google_token, "<--- token");
    try {
      const { email, name, username, imgProfile } = await googleVerify(
        google_token
      );
      let user = await User.findOne({ email });
      //TODO: REVISAR LOGIN Y ERRORES;
      if (user) {
        console.log("user already in the database");
        return res.json({
          status: 400,
          message: "User already in the data base",
          data: null,
        });
      }

      if (!user) {
        const userData = {
          name,
          username,
          email,
          password: "google_Auth10",
          imgProfile,
          google: true,
          state: true,
        };
        let user = await new User(userData);
        await user.save();

        if (user.password === "google_Auth10") {
          user.password = null;

          const token = jwt.sign(
            {
              uid: username._id,
              coworkings: user.coworkings,
              username: user.username,
              name: user.name,
              role: user.role,
              enterprise: user.enterprise
            },
            req.app.get("secretKey"),
            { expiresIn: "12h" }
          );

          return res.json({
            status: 200,
            message: HTTPCODE[200],
            data: { token: token, user: user },
          });
        }
      }
    } catch (err) {
      throw new Error(err);
    }
  },
};
