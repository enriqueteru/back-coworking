const Coworking = require("../models/Coworking.model");
const Service = require("../models/Service.model");
const HTTPCODE=require("../utils/httpsStatusCode");
const upload = require ("../services/updateServicesGallery.service");
const galleryUpload = upload.single("image");

module.exports={
getAllServices:async (req, res, next) => {
  try {
    if (req.query.page) {
      //Se le añade paginación
      const page = parseInt(req.query.page);
      const skip = (page - 1) * 20;
      const Services = await Service.find().skip(skip).limit(20);
      return res.json({
        status: 200,
        message: HTTPCODE[200],
        data: { Services: Services },
      });
    } else {
      const Services = await Service.find();
      return res.json({
        status: 200,
        message: HTTPCODE[200],
        data: { Services: Services },
      });
    }
  } catch (err) {
    return next(err);
  }
},

getServiceById:async (req, res, next) => {
  try {
    const { id } = req.params;
    const serviceById = await Service.findById(id);
    return res.json({
      status: 200,
      message: HTTPCODE[200],
      data: { Service: serviceById },
    });
  } catch (err) {
    return next(err);
  }
},
newService:async (req, res, next) => {
  try {
    const {name,description,price,icon,imgs,idCoworking} = req.body
    const newService = new Service({name,description,price,icon,imgs,idCoworking});
    const serviceDb = await newService.save();
    await Coworking.findByIdAndUpdate(idCoworking,{$addToSet:{services:serviceDb._id}},{new:true});
    return res.json({
      status: 201,
      message: HTTPCODE[201],
      data: { newService: serviceDb },
    });
  } catch (err) {
    return next(err);
  }
},
deleteService:async (req, res, next) => {
  try {
    const { id } = req.params;
    const serviceDeleted = await Service.findByIdAndDelete(id);
    if (!serviceDeleted) {
      return res.json({
        status: 404,
        message: HTTPCODE[404],
        data: null,
      });
    } else {
      return res.json({
        status: 200,
        message: HTTPCODE[200],
        data: { serviceDeleted: serviceDeleted },
      });
    }
  } catch (err) {
    return next(err);
  }
},

updateServiceById:async (req, res, next) => {
  try {
    const { id } = req.params;
    const {...all}=req.body;
    await Service.findByIdAndUpdate(id, all,{upsert:true});
    return res.json({
      status: 200,
      message: HTTPCODE[200],
      data: { update:all},
    });
  } catch (err) {
    return next(err);
  }
},

updateServicesGalleryById: async (req, res, next) => {
  try {
    const { id } = req.params;
    galleryUpload( req, res, function(err) {
        const service = async (id) => Service.findByIdAndUpdate(id, { imgs : req.file.location }, { new: true });
        service(id);
      return res.json({
        status: 200,
        messge: HTTPCODE[200],
        data: { image: req.file.location }
      })
    })
  } catch (err) {
    return next(err);
  }
}
}