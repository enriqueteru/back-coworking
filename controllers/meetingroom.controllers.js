const Coworking = require('../models/Coworking.model');
const MeetingRoom = require('../models/MeetingRoom.model');
const HTTPCODE = require("../utils/httpsStatusCode");
const upload = require("../services/updateMeetingRoomGallery.service");
const galleryUpload = upload.single("image");

module.exports = {
    newMeetingRoom: async (req, res, next) => {
        try {
            const { name, price, description, capacity, availability, idCoworking } = req.body;
            const newMeetingRoom = new MeetingRoom({ name, price, description, capacity, availability, idCoworking });
            const meetingRoomDb = await newMeetingRoom.save();
            await Coworking.findByIdAndUpdate(idCoworking,{$addToSet:{meetingRoom: meetingRoomDb._id}},{new:true});

            return res.json({
                status: 201,
                message: HTTPCODE[201],
                data: { newMeetingRoom: meetingRoomDb },
            });
        } catch (err) {
            return next(err);
        }
    },

    getAllMeetingRooms: async (req, res, next) => {
        try {
            if (req.query.page) {
                // paginación
                const page = parseInt(req.query.page);
                const skip = (page - 1) * 20;
                const MeetingRooms = await MeetingRoom.find().skip(skip).limit(20);
                return res.json({
                    status:200,
                    message: HTTPCODE[200],
                    data: { MeetingRoom: MeetingRooms },
                });
            } else {
                const MeetingRooms = await MeetingRoom.find();
                return res.json({
                    status: 200,
                    message: HTTPCODE[200],
                    data: { MeetingRoom: MeetingRooms }
                })
            }
        }
        catch (err) {
            return next(err);
        }
    },

    getMeetingRoomById: async (req, res, next) => {
        try {
            const { id } = req.params;
            const meetingRoomById = await MeetingRoom.findById(id);
            return res.json({
                status: 200,
                message: HTTPCODE[200],
                data: { meetingRoom: meetingRoomById },
            })
        } catch (err) {
            return next(err);
        }
    },

    updateMeetingRoomById: async (req, res, next) => {
        try {
            const { id } = req.params;
            const { ...all } = req.body;
            await MeetingRoom.findByIdAndUpdate(id, all, {upsert: true});
            return res.json({
                status: 200,
                message: HTTPCODE[200],
                data: { update: all }
            })
        } catch (err) {
            return next(err);
        }
    },
    deleteMeetingRoomById: async (req, res, next) => {
        try {
            const { id } = req.params;
            const meetingRoomDeleted = await MeetingRoom.findByIdAndDelete(id);
            if (!meetingRoomDeleted) {
                return res.json({
                    status: 418,
                    message: HTTPCODE[418], //"I'm a teapot"
                    data: null,
                })
            } else {
                return res.json({
                    status: 200,
                    message: HTTPCODE[200],
                    data: { MeetingRoom: meetingRoomDeleted }
                })
            }
        } catch (err) {
            return next(err)
        }
    },

    updateMeetingRoomGalleryById: async (req, res, next ) => {
        try {
        const { id } = req.params;
        galleryUpload(req, res, function(err) {
             const meetingroom = async (id) => MeetingRoom.findByIdAndUpdate(id, {  imgs: req.file.location }, {new: true});
                meetingroom(id);
            
            return res.json({
                status: 200,
                message: HTTPCODE[200],
                data: {
                    imgUrl: req.file.location
                }
            })
        })
    } catch (err) {
        return next(err);
    }
    }

}



