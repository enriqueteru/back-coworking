const User = require('../models/User.model');
const Coworking = require('../models/Coworking.model');
const { response } = require('express');

const allowCollection = [
    'coworking',
    'user'
];

const searchUser = async( term = '', res = response ) => {

    const regex = new RegExp( term, 'i' );
    const users = await User.find({
        $or: [{ name: regex }, { email: regex }, { username: regex }]
    });

    res.json({
        results: users
    });

}

const searchCoworking = async( term = '', res = response ) => {

    const regex = new RegExp( term, 'i' );
    const coworkings = await Coworking.find({ nameCoworking: regex });

    res.json({
        results: coworkings
    });

}

const search = ( req, res = response ) => {
    
    const { collection, term  } = req.params;

    if ( !allowCollection.includes( collection ) ) {
        return res.status(400).json({
            message: `Allow Collections: ${ allowCollection }`
        })
    }

    switch (collection) {
        case 'user':
            searchUser(term, res);
        break;
        case 'coworking':
            searchCoworking(term, res);
        break;
        
        
        default:
            res.status(500).json({
                message: 'Type a search please'
            })
    }

}



module.exports = {
    search
}