const Enterprise = require("../models/Enterprise.model");
const HTTPCODE = require("../utils/httpsStatusCode");
const upload = require("../services/updateEnterpriseGallery.service");
const galleryUpload = upload.single('image');

module.exports = {
  getAllEnterprises: async (req, res, next) => {
    try {
      if (req.query.page) {
        const page = parseInt(req.query.page);
        const skip = (page - 1) * 20;
        const Enterprises = await Enterprise.find()
          .populate("coworkings")
          .skip(skip)
          .limit(20)
          .populate("coworkings");
        return res.json({
          status: 200,
          message: HTTPCODE[200],
          data: { Enterprises: Enterprises },
        });
      } else {
        const Enterprises = await Enterprise.find().populate("coworkings");
        return res.json({
          status: 200,
          message: HTTPCODE[200],
          data: { Enterprises: Enterprises },
        });
      }
    } catch (err) {
      return next(err);
    }
  },
  getEnterpriseById: async (req, res, next) => {
    try {
      const { id } = req.params;
      const enterpriseById = await Enterprise.findById(id).populate("coworkings");
      return res.json({
        status: 200,
        message: HTTPCODE[200],
        data: { Enterprise: enterpriseById },
      });
    } catch (err) {
      return next(err);
    }
  },

  newEnterprises: async (req, res, next) => {
    try {
      const {
        corporationName,
     
        cif,
        description,
        logo,
        address,
        email,
        phone,
       
        img,
      } = req.body;
      const newEnterprise = new Enterprise({
        corporationName,
    
        cif,
        description,
        logo,
        address,
        email,
        phone,
 
        img,
      });
      const saved = await newEnterprise.save();
      console.log("New Enterprise", saved);
      return res.json({
        status: 201,
        message: HTTPCODE[201],
        data: { newEnterprise: newEnterprise },
      });
    } catch (err) {
      return next(err);
    }
  },
  deleteEnterprises: async (req, res, next) => {
    try {
      const { id } = req.params;
      const enterpriseDeleted = await Enterprise.findByIdAndDelete(id);
      if (!enterpriseDeleted) {
        return res.json({
          status: 418,
          message: HTTPCODE[418],
          data: null,
        });
      } else {
        return res.json({
          status: 200,
          message: HTTPCODE[200],
          data: { Enterprise: enterpriseDeleted },
        });
      }
    } catch (err) {
      return next(err);
    }
  },

  updateEnterpiseById: async (req, res, next) => {
    try {
      const { id } = req.params;
      const { ...all } = req.body;
      await Enterprise.findByIdAndUpdate(id, all, { upsert: true });
      return res.json({
        status: 200,
        message: HTTPCODE[200],
        data: { update: all },
      });
    } catch (err) {
      return next(err);
    }
  },

  updateEnterpriseGalleryById: async (req, res, next) => {
    try {
      const { id } = req.params;
        galleryUpload(req, res, function(err) {
        const enterprise = async (id) => Enterprise.findByIdAndUpdate(id, { logo : req.file.location }, { new: true });
        enterprise(id);
          
          return res.json({
            status: 200,
            message: HTTPCODE[200],
            data: {
              image : req.file.location
            }
          })
        })
    } catch (err) {
      return next (err);
    }
  }
};
