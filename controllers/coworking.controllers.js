const Coworking = require("../models/Coworking.model");
const Enterprise=require("../models/Enterprise.model");
const  HTTPCODE=require("../utils/httpsStatusCode");
const upload = require('../services/uploadCoworkingGallery.service')
const galleryUpload = upload.single('image');

module.exports={
getAllCoworkings:async (req, res, next) => {
  try {

    const totalCoworkins = await Coworking.count()  //TOTAL USER
    if (req.query.page) {
      //Add pagination
      const items = parseInt(req.query.items);
      const page = parseInt(req.query.page);
      const skip = (page - 1) * 20;
      const Coworkings = await Coworking.find()
        .skip(skip)
        .limit(items || 20) 
        //query para hacer listas concretas
        .populate("enterprise","id,name,logo")
        /*.populate("meetingRoom", "id, name, capacity")*/
        .populate("workingSpaces", "id, name, capacity")
        .populate("services", "id, name");
      return res.json({
        status: 200,
        message: HTTPCODE[200],
        data: { Coworkings: Coworkings,
        totalCoworkins },
      });
    } else {
      const Coworkings = await Coworking.find()
      .populate("enterprise","id,name,logo")
      /*.populate("meetingRoom", "id, name, capacity") //TODO: MEETINGROOMS  */
      .populate("workingSpaces", "id, name, capacity")
      .populate("services", "id, name");
      return res.json({
        status: 200,
        message: HTTPCODE[200],
        data: { Coworkings: Coworkings,
        totalCoworkins},

      });
    }
  } catch (err) {
    return next(err);
  }
},

getCoworkingById:async (req, res, next) => {
  try {
    const { id } = req.params;
    const coworkingById = await Coworking.findById(id).populate("enterprise","id,name,logo")/*.populate("meetingRoom", "id, name, capacity") //TODO: MEETINGROOMS  */.populate("workingSpaces", "id, name, capacity").populate("services", "id, name");
    return res.json({
      status: 200,
      message: HTTPCODE[200],
      data: { Coworking: coworkingById },
    });
  } catch (err) {
    return next(err);
  }
},

newCoworking: async (req, res, next) => {
  try {
    const { enterprise, nameCoworking, phone, email, logo, imgs, description, address,city,meetingRooms, workingSpaces, services } = req.body;
    const newCoworking = new Coworking({ enterprise, nameCoworking, phone, email, logo, imgs, description, address,city,meetingRooms, workingSpaces, services } );
    const coworkingDb = await newCoworking.save();
    await Enterprise.findByIdAndUpdate(enterprise,{$addToSet:{coworking:coworkingDb._id}},{new:true});

    return res.json({
      status: 201,
      message: HTTPCODE[201],
      data: { newCoworking: coworkingDb },
    });
  } catch (err) {
    return next(err);
  }
},
deleteCoworking:async (req, res, next) => {
  try {
    const {id} = req.params;
    if (id) {
      const coworkingDeleted = await Coworking.findByIdAndDelete(id);
      if (!coworkingDeleted) {
        return res.json({
          status: 404,
          message: HTTPCODE[404],
          data: null,
        });
      } else {
        return res.json({
          status: 200,
          message: HTTPCODE[200],
          data: { CoworkingDelete: coworkingDeleted },
        });
      }
    } else {
      return res.json({
        status: 404,
        message: HTTPCODE[404],
        data: { CoworkingDelete: coworkingDeleted },
      });
    }
  } catch (err) {
    return next(err);
  }
},

updateCoworkingById: async (req, res, next) => {
  try {
    const { id } = req.params;
    const{...all}=req.body;
    await Coworking.findByIdAndUpdate(id, all,{upsert:true});
    return res.json({
      status: 200,
      message: HTTPCODE[200],
      data: { update:all},
    });
  } catch (err) {
    return next(err);
  }
},

updateCoworkingGalleryById: async (req, res, next) => {
  try {
    const { id } = req.params;
      galleryUpload(req, res, function(err) {
         const coworking = async (id) => Coworking.findByIdAndUpdate(id, {  imgs: req.file.location  }, {new: true});
          coworking(id);
    
        return res.json({
          status: 200,
          message: HTTPCODE[200],
          data: { imgUrl: req.file.location }
        })
      })
  } catch (err) {
    return next(err);
  }
}
}

