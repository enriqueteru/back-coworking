const Coworking = require("../models/Coworking.model");
const WorkingSpace = require("../models/workingSpaces.model");
const HTTPCODE=require("../utils/httpsStatusCode");
const upload = require("../services/updateWorkingSpace.service");
const galleryUpload = upload.single('image');

module.exports = {
getAllWorkingSpaces: async (req, res, next) => {
  try {
    if (req.query.page) {
      const page = parseInt(req.query.page);
      const skip = (page - 1) * 20;
      const WorkingSpaces = await WorkingSpace.find().skip(skip).limit(20);
      return res.json({
        status: 200,
        message: HTTPCODE[200],
        data: { WorkingSpaces: WorkingSpaces },
      });
    } else {
      const WorkingSpaces = await WorkingSpace.find();
      return res.json({
        status: 200,
        message: HTTPCODE[200],
        data: { WorkingSpaces: WorkingSpaces },
      });
    }
  } catch (err) {
    return next(err);
  }
},

getWorkingSpacesById: async (req, res, next) => {
  try {
    const { id } = req.params;
    const workingSpacesById = await WorkingSpace.findById(id);
    return res.json({
      status: 200,
      message: HTTPCODE[200],
      data: { workingSpaces: workingSpacesById },
    });
  } catch (err) {
    return next(err);
  }
},
newWorkingSpaces:async (req, res, next) => {
  try {
    const {name,description,capacity,price,availability,idCoworking} = req.body;
    const newWorkingSpaces = new WorkingSpace({name,description,capacity,price,availability,idCoworking});
    const workingSpacesDb = await newWorkingSpaces.save();
    await Coworking.findByIdAndUpdate(idCoworking,{$addToSet:{workingSpace:workingSpacesDb._id}},{new:true});
    return res.json({
      status: 201,
      message: HTTPCODE[201],
      data: { newWorkingSpaces: workingSpacesDb },
    });
  } catch (err) {
    return next(err);
  }
},
deteleWorkingSpaces: async(req,res,next) =>{
  try{
    const {id} = req.params;
      const workingSpacesDeleted = await WorkingSpace.findByIdAndDelete(id);
      if(!workingSpacesDeleted){
        return res.json({
          status:404,
          message: HTTPCODE[404],
          data:null,
        });
      }else{
      return res.json({
        status:200,
        message:HTTPCODE[200],
        data:{WorkingSpace: workingSpacesDeleted},
      });
    }
  } catch(err){
    return next(err);
  }
},
updateWorkingSpaceById: async(req, res, next)=>{
 try{
   const {id}= req.params;
   const{...all}= req.body;
   await WorkingSpace.findByIdAndUpdate(id,all,{upsert:true});
   return res.json({
     status:200,
     message:HTTPCODE[200],
     data:{update:all},
   });
 }
 catch (err){
   return next(err)
 }
},

updateWorkingspaceGalleryById: async (req, res, next) => {
  try {
    const { id } = req.params;
    galleryUpload(req, res, function (err) {
        const workingspace = async (id) => WorkingSpace.findByIdAndUpdate(id, {  imgs: req.file.location }, {new: true});
        workingspace(id);
      return res.json({
        status: 200,
        message: HTTPCODE[200],
        data: { image: req.file.location }
      })
    })
  } catch (err) {
    return next(err);
  }
}
}
