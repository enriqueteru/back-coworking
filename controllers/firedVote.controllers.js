const firedVote = require("../models/firedVote.model");
const HTTPCODE = require("../utils/httpsStatusCode");


module.exports = {
    getfiredVotes: async (req, res, next) => {
        try {
          let Votes = await firedVote.find();
          return res.json({
            status: 200,
            message: HTTPCODE[200],
            votes: Votes
            
          });
        } catch (err) {
          return next(err);
        }
      },

      updateVoteByName: async(req, res, next) => {
        try {
        
          const { name } = req.params;
         const {...all}=req.body;
         let vote = await firedVote.findOneAndUpdate({name}, { $inc: { vote: 1 } });
          return res.json({
            status: 200,
            message: HTTPCODE[200],
            data: { vote},
          });
        } catch (err) {
          return next(err);
        }
      }
    
    
    
    
    }
    