const Coworking = require("../models/Coworking.model");
const Enterprise = require("../models/Enterprise.model");
const MeetingRoom = require("../models/MeetingRoom.model");
const Role = require("../models/role.model");
const Service = require("../models/Service.model");
const User = require("../models/User.model");
const WorkingSpaces = require("../models/workingSpaces.model");

const roleValidator = async (role = "USER_ROLE") => {
    const existRoleType = await Role.findOne({ role });
    if (!existRoleType) {
      {
        throw new Error(` 
          this ${role} type not exist in the app`);
      }
    }
  }

   //if
    const emailValidatorUser = async( email = '' ) => {
              const emailExist = await User.findOne({ email });
        if ( emailExist ) {
            throw new Error(` ${ email } is already registered in the DB`);
        }
    }



const userByIdvalid = async( id ) => {
    
    const userExist = await User.findById(id);
    if ( !userExist ) {
        throw new Error(`this ${ id } does not belong to any user of this app`);
    }
}

const nameCoworkingValidator = async( nameCoworking = '' ) => {
  const nameCoworkingExist = await Coworking.findOne({ nameCoworking });
  if ( nameCoworkingExist ) {
      throw new Error(` ${ nameCoworking } is already registered in the DB`);
  }
}
const emailValidatorCoworking = async( email = '' ) => {
  const emailExist = await Coworking.findOne({ email });
  if ( emailExist ) {
      throw new Error(` ${ email } is already registered in the DB`);
  }
}
const nameWorkingSpaceValidator = async( name = '' ) => {
  
  const nameWorkingSpaceExist = await WorkingSpaces.findOne({ name });
  if ( nameWorkingSpaceExist ) {
      throw new Error(` ${ name } is already registered in the DB`);
  }
}

const nameEnterpriseValidator = async( name = '' ) => {
  
  const nameEnterpriseExist = await Enterprise.findOne({ name });
  if ( nameEnterpriseExist ) {
      throw new Error(` ${ name } is already registered in the DB`);
  }
}

const nameServiceValidator = async (name = '') => {
  const nameServiceExist = await Service.findOne({ name });
  if (nameServiceExist) {
    throw new Error(`The Service name ${name}, is already in use`);
  }
}

const nameMeetingRoomValidator = async (name = '') => {
  const nameMeetingRoomExist = await MeetingRoom.findOne({ name });
  if (nameMeetingRoomExist) {
    throw new Error(`The Meeting Room name ${name}, is already in use`)
  }
}



  module.exports = { 
      roleValidator,
      emailValidatorUser,
      userByIdvalid,
      nameCoworkingValidator,
      emailValidatorCoworking,
      nameWorkingSpaceValidator,
      nameEnterpriseValidator,
      nameServiceValidator,
      nameMeetingRoomValidator
  }