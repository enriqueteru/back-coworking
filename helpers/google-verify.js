const { OAuth2Client } = require("google-auth-library");
const client = new OAuth2Client(
  "987710553830-6ifm6iscksb8q34gqnip98lt29e8h3n7.apps.googleusercontent.com"
);

//TODO: VARIABLE DE ENTORNO --> GOOGLE ID CLIENT
async function googleVerify(token = "") {
  const ticket = await client.verifyIdToken({
    idToken: token,
    audience:
      "987710553830-6ifm6iscksb8q34gqnip98lt29e8h3n7.apps.googleusercontent.com",
  });

  const {
    name,
    sub: username,
    picture: imgProfile,
    email,
  } = ticket.getPayload();

  return { name, username, imgProfile, email };
}

module.exports = {
  googleVerify,
};
