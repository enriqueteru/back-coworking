const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const enterpriseSchema = new Schema(
  {
    corporationName: { type: String, require: true },
    cif: { type: String },
    description: { type: String, require: true },
    logo: { type: String, require: false },
    address: { type: String, require: false },
    city: { type: String, require: false },
    email: { type: String, require: false },
    phone: { type: Number, require: true },
    imgs: [{ type: String, require: false }],
    coworkings: [
      {
        type: Schema.Types.ObjectId,
        ref: "Coworking",
        required: false,
      },
    ],
  },

  { timestamps: true }
);

const Enterprise = mongoose.model("Enterprise", enterpriseSchema);
module.exports = Enterprise;

