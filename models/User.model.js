const mongoose = require("mongoose");
const Schema = mongoose.Schema;


const userSchema = new Schema(
  {
    username: { type: String, required: [true, 'no puede ir vacio'], index: true, unique: true},
    name: { type: String, required: true},
    surname: { type: String },
    docIdentity: { type: String, required: true },
    imgProfile: { type: String, default: 'https://proyecto-coworking.s3.eu-west-1.amazonaws.com/profile/profile-default.png'},
    email: { type: String, required: [true, 'This email is already taken'] },
    address: { type: String },
    phone: { type: String },
    enterprise: { type: Schema.Types.ObjectId, ref: "Enterprise", required: true, default:"623e2c8e62c842ae01e2d9be"},
    password: { type: String, required: true },
    state: { type: Boolean, default: true },
    google: { type: Boolean, default: false },
    role: {
      enum: ["ADMIN_ROLE", "ENTERPRISE_ROLE", "USER_ROLE"],
      type: String,
      default: "USER_ROLE",
      required: true,
    },
    coworkings: [
      { type: Schema.Types.ObjectId, ref: "Coworking", required: false },
    ],
  },
  {
    timestamps: true,
  }
);

userSchema.methods.toJSON = function() {
  const { __v,  _id, ...user  } = this.toObject();
  user.uid = _id;
  return user;
}
// esto borra de la base de datos la contraseña, por seguridad ( es mejor no guardar esa info en la BD)
//y cambia el nombre del esquema visualmente solo a _id, para guiarnos mejor


const User = mongoose.model("User", userSchema);
module.exports = User;

