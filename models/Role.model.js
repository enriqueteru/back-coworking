const { Schema, model } = require('mongoose');

const roleSchema = Schema({
    role: {
        type: String,
        required: [true, 'Role is Required']
    }
});


module.exports = model( 'Role', roleSchema );
