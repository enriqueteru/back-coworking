const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const meetingRoomSchema = new Schema (
{
    name: {type: String, require: true},
    price: {type: Number, require: true},
    description: {type: String, require: false},
    capacity: {type: Number, require: true},
    availability: {type: Boolean, require: false},
    idCoworking: { type: Schema.Types.ObjectId, ref: "Coworking", required: true },
    imgs: { type: [String], require: false },
},

{ timestamps: true}

);

const MeetingRoom = mongoose.model("MeetingRoom", meetingRoomSchema);
module.exports = MeetingRoom;