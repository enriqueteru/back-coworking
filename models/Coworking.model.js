const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const coworkingSchema = new Schema(
  {
    enterprise: {
      type: Schema.Types.ObjectId,
      ref: "Enterprise",
      required: true,
    },
    nameCoworking: { type: String, require: true },
    phone: { type: Number, require: true },
    email: { type: String, require: true },
    logo: { type: String, require: false },
    imgs: { type: [String], require: false },
    description: { type: String, require: false },
    address: {type: String, require: false },
    city: {type: String, require: false },

    meetingRooms: [
      { type: Schema.Types.ObjectId, ref: "MeetingRoom", required: false },
    ],
    workingSpaces: [
      { type: Schema.Types.ObjectId, ref: "WorkingSpace", required: false },
    ],
    services: [
      { type: Schema.Types.ObjectId, ref: "Service", required: false },
    ],
  },

  { timestamps: true }
);

const Coworking = mongoose.model("Coworking", coworkingSchema);
module.exports = Coworking;
