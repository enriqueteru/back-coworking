const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const serviceSchema = new Schema(
  {
    name: { type: String, require: true },
    description: { type: String, require: true },
    price: { type: Number, require: true },
    logo: { type: String, require: false },
    imgs: { type: [String], require: false },
    idCoworking: { type: Schema.Types.ObjectId, ref: "Coworking", required: true },
  },
  { timestamps: true }
);

serviceSchema.methods.toJSON = function() {
  const { __v, password,_id, ...service  } = this.toObject();
  service.sid = _id;
  return service;
}

const Service = mongoose.model("Service", serviceSchema);
module.exports = Service;

