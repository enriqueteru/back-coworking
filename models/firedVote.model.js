const { Schema, model } = require('mongoose');

const firedVoteSchema = Schema({
    vote: {
        type: Number,
        default: 0
    },
    name: {
        type: String
    }
});


module.exports = model( 'firedVote', firedVoteSchema );
