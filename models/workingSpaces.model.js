const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const workingSpacesSchema = new Schema(
{
name:{type:String,require:true},
description:{type:String,require:false},
capacity:{type:Number,require:true},
price:{type:Number,require:true},
availability:{type:Boolean,require:false},
imgs: { type: [String], require: false },
idCoworking: { type: Schema.Types.ObjectId, ref: "Coworking", required: true },

},
{timestamps:true}
);

const WorkingSpaces = mongoose.model("WorkingSpace", workingSpacesSchema);
module.exports = WorkingSpaces;
