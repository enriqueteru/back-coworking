
const isThatRole = ( ...roles  ) => { //...roles = a todo lo que venga por parametro cuando esta en argumento
    return (req, res, next) => {
        
        if ( !req.authority ) {
            return res.status(500).json({
                msg: 'impossible to verify role without validating token first'
            });
        }
 
    const {role} = req.authority;

        if ( !roles.includes( role ) ) {
            return res.status(401).json({
                msg: `This service require one of this: ${ roles }`
            });
        }
        next();
    }
}


module.exports = { 
isThatRole}
