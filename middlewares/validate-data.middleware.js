const { validationResult } = require("express-validator");

//ESTE MIDDLEWARE ES NECESARIO TRAS TODOS LOS CHECK DE VALIDATOR EXPRESS PARA RECIBIR POR RES LOS ERRORES SI LOS HUBIERA, SI NO APARECE EN LA ULTIMA POSICION DEL ARRAYA DE MIDDLEWARE DE LA RUTAS; O FUNCIONARA EL VALIDATOR
const validateData = (req, res, next) => {
  const errorsValidationMiddleware = validationResult(req);
  if (!errorsValidationMiddleware.isEmpty()) {
    return res.status(400).json(errorsValidationMiddleware);
  }
  next();
};

module.exports = {
  validateData,
};
