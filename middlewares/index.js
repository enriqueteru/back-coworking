
const validateJWT = require("../middlewares/authenticated.middleware");
const validateData= require("../middlewares/validate-data.middleware");
const validateRole = require("../middlewares/validate-role.middleware");

module.exports = { 
    ...validateJWT,
    ...validateData,
    ...validateRole
};