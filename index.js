const express = require("express");
const app = express();
const path = require("path");
require('dotenv').config()


//DB CONFIG
const { connect } = require("./config/db");
connect();
const PORT = process.env.PORT


// SET HEADERS CRUD
app.use((req, res, next) => {
  res.header("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE");
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Content-Type");
  next();
});

// CORS
const cors = require("cors");

app.use(
  cors({
    origin: ["http://localhost:3002, http://localhost:3000, https://back-coworking.herokuapp.com/"],
    credentials: true,
  })
);

//JSON APPLIED
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

//JSON WEB TOKEN KEY
app.set("secretKey", process.env.SECRET_KEY_JWT); 

//PUBLIC FOLDER
app.use("/public", express.static(path.join(__dirname, "./public")));

//ROUTES
app.get('/', function(req, res){
    res.status(200).sendFile(path.join(__dirname,'./views/index.html'));
  })

  //-ENTERPRISE
  const entreprise = require("./routes/enterprise.routes");
  app.use("/entreprise", entreprise);
//|
//|
  //-SEARCH ROUTE
  const search = require("./routes/search.routes");
  app.use("/search", search);
//|
//|
  //-FIREDVOTES
  const firedVotes = require("./routes/firedVotes.routes");
  app.use("/votes", firedVotes);
//|
  //-USERS
  const user = require("./routes/user.routes");
  app.use("/user", user);
  //-AUTH
  const auth = require("./routes/auth.routes");
  app.use("/auth", auth);
  
  //-COWORKINGS
    const coworking = require("./routes/coworking.routes");
    app.use("/coworking", coworking);
    //|
    //|
    //|
      //-SERVICES
    const service = require("./routes/services.routes");
    app.use("/service", service);
    //|
    //|
    //|
       //-WORKSPACES
    const workingSpace = require("./routes/workingSpace.routes");
    app.use("/workingspace", workingSpace);
    //|
    //|
      //-MEETINGROOM
    const meetingroom = require("./routes/meetingRooms.routes");
    app.use("/meetingroom", meetingroom);
    //


//ERROR
app.use((err, req, res, next) => {
  return res.status(err.status || 500).json(err.message || "Unexpected error");
});

//SERVER LISTEN
app.disable("x-powered-by");

app.listen(PORT, () => {
  console.log(`Server started on http://localhost:${PORT}`);
});