const aws = require('aws-sdk');
const multer = require('multer');
const multerS3 = require('multer-s3');

aws.config.update({
    secretAccessKey: process.env.SECRETACCESSKEY ,
    accessKeyId: process.env.ACCESSKEYID ,
    region:  process.env.REGION
});

const s3 = new aws.S3();

const upload = multer({
    storage: multerS3({
        s3: s3,
        bucket: 'proyecto-coworking/services',
        acl: 'public-read',
        metadata: function (req, file, cb) {
            cb(null, {fieldName: 'images'});
        },
        key: function (req, file, cb) {
            cb(null, Date.now().toString() + '.jpg')
        }
    })
})

module.exports = upload;