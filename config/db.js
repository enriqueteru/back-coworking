const mongoose = require('mongoose');

//const DB_URL ="mongodb+srv://proyectoupgrade:UQ1C2p6g1keqC3XO@cluster0.ctd7p.mongodb.net/coworking";
const DB_URL =process.env.DB_URL;

console.log('DB_URL:', DB_URL);

const connect = async() => {

  try{
    await mongoose
    .connect(DB_URL, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });

    console.log("Connecting to DB OK: ", DB_URL);

  }catch(err){ 
    console.log("Error connecting to DB: ", err);

  }
}

module.exports = { DB_URL, connect };