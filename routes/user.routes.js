const express = require("express");
const router = express.Router();
const { check } = require("express-validator");

//HELPERS EXISTE TIPO DE ROL/ EL EMAIL ESTA EN USO? / EXISTE ESE USERPOR ID
const { roleValidator, emailValidatorUser } = require("../helpers/db-validations");

//MIDDLEWARES VALIDACIONES CUSTOM ROLES / TOKEN JWT / VALIDACIONES EXPRESS-VALIDATOR
const { validateData, isAuth, isThatRole } = require("../middlewares");

//CONTROLLERS
const userControllers = require("../controllers/user.controllers");

router.get(
  "/",
  [isAuth],
  userControllers.getAllUsers
);
router.get(
  "/:id",
  [check("id", "Not a valid ID in this app!!!").isMongoId(), validateData],
  userControllers.getUserById
);

router.post(
  "/new",
  [
    check("username", "Username is required").not().isEmpty(),
    check("email", "Not a valid Email").isEmail(),
    check("password", "Please enter a password at least 8 character.").isLength(
      { min: 8 }
    ),
    check("password", "Please enter a strong password").matches(
      /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[a-zA-Z\d@$.!%*#?&]/
    ),
    check("role").custom(roleValidator),
    check("email").custom(emailValidatorUser),
    validateData,
  ],
  userControllers.newUser
);

router.put("/update/:id", [isAuth, isThatRole("ADMIN_ROLE", "ENTERPRISE_ROLE")], userControllers.updateUserById);
router.delete("/delete/:id",[isAuth, isThatRole("ADMIN_ROLE", "ENTERPRISE_ROLE")]  ,userControllers.deleteUser);
router.put("/update/:id/profilePic",/* [isAuth, check("id", "Not is a valid ID!").isMongoId(), validateData],*/ userControllers.updateProfilePic);


module.exports=router;
