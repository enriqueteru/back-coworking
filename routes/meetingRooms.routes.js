//TO DO ROUTES OF MEETINGROOMS
const express = require('express');
const router = express.Router();
const meetingRoomRoutes = require('../controllers/meetingroom.controllers');
const { isAuth, isThatRole, validateData } = require('../middlewares');
const { nameMeetingRoomValidator } = require('../helpers/db-validations');
const { check } = require('express-validator');


router.get('/', meetingRoomRoutes.getAllMeetingRooms);

router.post('/new', [isAuth, isThatRole("ADMIN_ROLE", "ENTERPRISE_ROLE"), check("name").custom(nameMeetingRoomValidator), validateData], meetingRoomRoutes.newMeetingRoom);

router.get('/:id', [check("id", "Not is a valid ID!").isMongoId() ,validateData], meetingRoomRoutes.getMeetingRoomById);

router.put('/update/:id', [isAuth, isThatRole("ADMIN_ROLE", "ENTERPRISE_ROLE"), check("id", "Not is a valid ID!").isMongoId(), validateData], meetingRoomRoutes.updateMeetingRoomById);

router.delete('/delete/:id', [isAuth, isThatRole("ADMIN_ROLE", "ENTERPRISE_ROLE"), check("id", "Not is a valid ID!").isMongoId(), validateData], meetingRoomRoutes.deleteMeetingRoomById);

router.post('/update/:id/gallery', [isAuth, isThatRole("ADMIN_ROLE", "ENTERPRISE_ROLE"), check("id", "Not is a valid ID!").isMongoId(), validateData], meetingRoomRoutes.updateMeetingRoomGalleryById);

module.exports = router; 