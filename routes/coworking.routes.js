const express = require('express');
const { check } = require('express-validator');
const router = express.Router();

const coworkingControllers = require('../controllers/coworking.controllers');
const { isAuth, validateData, isThatRole, } = require('../middlewares');
const {nameCoworkingValidator, emailValidatorCoworking}=require("../helpers/db-validations")



router.get('/',coworkingControllers.getAllCoworkings);
router.get('/:id',
[check("id", "Not a valid ID in this app!!!").isMongoId(), validateData],
coworkingControllers. getCoworkingById);
router.post('/new',
[isAuth,
check("nameCoworking", "nameCoworking is required").not().isEmpty(),
check("nameCoworking").custom(nameCoworkingValidator),
check("email").custom(emailValidatorCoworking),

validateData
],coworkingControllers.newCoworking);
router.put('/update/:id',
[isAuth,
isThatRole("ADMIN_ROLE", "ENTERPRISE_ROLE"),
check("id", "Not a valid ID in this app!!!").isMongoId(), validateData
],coworkingControllers.updateCoworkingById);
router.delete('/delete/:id',
[isAuth,
isThatRole("ADMIN_ROLE", "ENTERPRISE_ROLE"),
check("id", "Not a valid ID in this app!!!").isMongoId(), validateData
], coworkingControllers.deleteCoworking);

router.post("/update/:id/gallery", [isAuth, isThatRole("ADMIN_ROLE", "ENTERPRISE_ROLE"), check("id", "Not is a valid ID!").isMongoId(), validateData], coworkingControllers.updateCoworkingGalleryById );


module.exports = router;
