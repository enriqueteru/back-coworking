const express = require("express");
const { check } = require("express-validator");
const router = express.Router();
const authRoutes = require("../controllers/auth.controllers");
const { roleValidator, emailValidatorUser } = require("../helpers/db-validations");
const { validateData, isAuth } = require("../middlewares/");


router.get("/renew", isAuth ,authRoutes.renewToken )
router.post("/logout", authRoutes.logoutAuth);
router.post("/register",
[
  check("username", "Username is required").not().isEmpty(),
  check("email", "Not a valid Email").isEmail(),
  check("password", "Please enter a password at least 8 character.").isLength(
    { min: 8 }
  ),
  check("password", "Please enter a strong password").matches(
    /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[a-zA-Z\d@$.!%*#?&]/
  ),
  check("role").custom(roleValidator),
  check("email").custom(emailValidatorUser),
  validateData,
],

authRoutes.registerAuth);
router.post("/authenticated", authRoutes.authenticatedAuth);
router.post( 
  "/google",
  [
    check("google_token", "google_token is required").not().isEmpty(),
    validateData,
  ],
  authRoutes.googleSignIn
);

module.exports = router;
