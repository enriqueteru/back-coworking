const express = require('express');
const router = express.Router();
const serviceControllers= require('../controllers/service.controllers');
const { check } = require("express-validator");
const { validateData, isAuth, isThatRole } = require('../middlewares')
const {nameServiceValidator} = require('../helpers/db-validations')

router.get('/', serviceControllers.getAllServices);

router.get('/:id', [check("id", "Not is a valid ID!").isMongoId(), validateData], serviceControllers.getServiceById);

router.post('/new', [isAuth, isThatRole("ADMIN_ROLE", "ENTERPRISE_ROLE"), check("name").custom(nameServiceValidator), validateData], serviceControllers.newService);

router.put('/update/:id', [isAuth, isThatRole("ADMIN_ROLE", "ENTERPRISE_ROLE"), check("id", "Not is a valid ID!").isMongoId(), validateData], serviceControllers.updateServiceById);

router.delete('/delete/:id', [isAuth, isThatRole("ADMIN_ROLE", "ENTERPRISE_ROLE"), check("id", "Not is a valid ID!").isMongoId(), validateData], serviceControllers.deleteService);

router.post('/update/:id/gallery', [isAuth, isThatRole("ADMIN_ROLE", "ENTERPRISE_ROLE"), check("id", "Not is a valid ID!").isMongoId(), validateData], serviceControllers.updateServicesGalleryById);

module.exports = router;