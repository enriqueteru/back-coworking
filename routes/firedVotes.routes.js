const express = require("express");
const firedVoteControllers = require("../controllers/firedVote.controllers");
const router = express.Router();


router.get(
    "/all",
    firedVoteControllers.getfiredVotes
  );
router.get(
    "/:name",
    firedVoteControllers.updateVoteByName
  );

module.exports = router;
