const express = require("express");
const router = express.Router();

const workingSpaceRoutes = require("../controllers/workingSpace.controller");
const { isAuth, validateData, isThatRole } = require("../middlewares");
const { nameWorkingSpaceValidator,} = require("../helpers/db-validations");
const { check } = require("express-validator");
const workingSpaceController = require("../controllers/workingSpace.controller");

router.get("/", workingSpaceRoutes.getAllWorkingSpaces);
router.get(
  "/:id",
  [check("id", "Not a valid ID in this app!!!").isMongoId(), validateData],
  workingSpaceRoutes.getWorkingSpacesById
);
router.post(
  "/new",
  [
    isAuth,
    isThatRole("ADMIN_ROLE", "ENTERPRISE_ROLE"),
    check("name").custom(nameWorkingSpaceValidator),
    validateData,
  ],
  workingSpaceRoutes.newWorkingSpaces
);
router.put(
  "/update/:id",
  [
    isAuth,
    isThatRole("ADMIN_ROLE", "ENTERPRISE_ROLE"),
    check("id", "Not a valid ID in this app!!!").isMongoId(),
    validateData,
  ],
  workingSpaceRoutes.updateWorkingSpaceById
);
router.delete(
  "/delete/:id",
  [
    isAuth,
    isThatRole("ADMIN_ROLE", "ENTERPRISE_ROLE"),
    check("id", "Not a valid ID in this app!!!").isMongoId(),
    validateData,
  ],
  workingSpaceRoutes.deteleWorkingSpaces
);

router.post(
  "/update/:id/gallery",
  [
    isAuth,
    isThatRole("ADMIN_ROLE", "ENTERPRISE_ROLE"),
    check("id", "Not a valid ID in this app!!!").isMongoId(),
    validateData,
  ],
  workingSpaceController.updateWorkingspaceGalleryById
);

module.exports = router;
