const express = require("express");
const router = express.Router();

const enterpriseControllers = require("../controllers/enterprise.controllers");
const { isAuth, isThatRole, validateData } = require("../middlewares");
const { nameEnterpriseValidator } = require("../helpers/db-validations");
const { check } = require("express-validator");

router.get(
  "/",
  [isAuth, isThatRole("ADMIN_ROLE", "ENTERPRISE_ROLE"), validateData],
  enterpriseControllers.getAllEnterprises
);

router.get(
  "/:id",
  [
    isAuth,
    isThatRole("ADMIN_ROLE", "ENTERPRISE_ROLE"),
    check("id", "Not a valid ID in this app!!!").isMongoId(),
    validateData,
  ],
  enterpriseControllers.getEnterpriseById
);

router.post(
  "/new",
  [
    isAuth,
    isThatRole("ADMIN_ROLE"),
    check("name").custom(nameEnterpriseValidator),
    validateData,
  ],
  enterpriseControllers.newEnterprises
);

router.put(
  "/update/:id",
  [
    isAuth,
    isThatRole("ADMIN_ROLE"),
    check("id", "Not a valid ID in this app!!!").isMongoId(),
    validateData,
  ],
  enterpriseControllers.updateEnterpiseById
);

router.delete(
  "/delete/:id",
  [
    isAuth,
    isThatRole("ADMIN_ROLE"),
    check("id", "Not a valid ID in this app!!!").isMongoId(),
    validateData,
  ],
  enterpriseControllers.deleteEnterprises
);

router.post("/upload/:id/gallery", [isAuth, isThatRole("ADMIN_ROLE", "ENTERPRISE_ROLE"), check("id", "Not is a valid ID!").isMongoId(), validateData], enterpriseControllers.updateEnterpriseGalleryById )

module.exports = router;
